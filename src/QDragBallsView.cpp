// ---------------------------------------------------------------------------------------------------------
#include <QDragBallsView.h>
// ---------------------------------------------------------------------------------------------------------
QDragBallsView::QDragBallsView( QWidget * parent_p ) :
   QGraphicsView	( parent_p  ),
   mouseDown_m    ( false     ),
   scale_m			( 100       ),
   mousePosition_m( 0, 0      ),
   sceneCenter_m  ( 0, 0      ),
   scene_m			( this      )
{	
   setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
   setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
   setRenderHint( QPainter::Antialiasing ); 
   setCursor( Qt::OpenHandCursor );
   setScene( &scene_m );	
   centerOn( 0, 0 );
}
// ---------------------------------------------------------------------------------------------------------
void QDragBallsView::checkSceneSize()
{
   const QSizeF   geomerySize_cl = QSizeF( geometry().size() ) * 100 / scale_m,
                  halfGeomerySize_cl = geomerySize_cl / 2;
   double maxDistance_cl = scene_m.powerToDistance( 1 ) * 100 / scale_m;
   const qreal sceneX_l = sceneCenter_m.x() - halfGeomerySize_cl.width() - maxDistance_cl,
               sceneY_l = sceneCenter_m.y() - halfGeomerySize_cl.height() - maxDistance_cl;
   maxDistance_cl *= 2;
   //
   bool update_l = false;
   QRectF sceneRect_l = scene_m.sceneRect();
   if ( sceneRect_l.x() > sceneX_l )              
   {  sceneRect_l.setX( sceneX_l ); update_l = true; }
   if ( sceneRect_l.y() > sceneY_l )
   {  sceneRect_l.setY( sceneY_l ); update_l = true; }
   const double   width_cl  = geomerySize_cl.width() + maxDistance_cl,
      height_cl = geomerySize_cl.height() + maxDistance_cl;
   if ( sceneRect_l.width() < width_cl )      
   {  sceneRect_l.setWidth( width_cl ); update_l = true; }
   if ( sceneRect_l.height() < height_cl )
   {  sceneRect_l.setHeight( height_cl ); update_l = true; }
   if ( update_l )
   {  scene_m.setSceneRect( sceneRect_l ); update(); }
   centerOn( sceneCenter_m );
}
// ---------------------------------------------------------------------------------------------------------
void QDragBallsView::mousePressEvent ( QMouseEvent * mouseEvent_p )
{
   if ( Qt::LeftButton == mouseEvent_p->button() ) 
   {
      mousePosition_m = mouseEvent_p->pos();
      mouseDown_m = true;
   }
   QGraphicsView::mousePressEvent( mouseEvent_p );
   setCursor( Qt::ClosedHandCursor );
}
// ---------------------------------------------------------------------------------------------------------
void QDragBallsView::mouseMoveEvent ( QMouseEvent * mouseEvent_p )
{
   if ( mouseDown_m && !scene().clickingHit() ) 
   {
      if ( geometry().contains( mouseEvent_p->pos() ) )
      {         
         sceneCenter_m += ( mousePosition_m - mouseEvent_p->posF() ) * 100 / scale_m;
         mousePosition_m = mouseEvent_p->posF();
         centerOn( sceneCenter_m );
      }
      else endMouseDruging();
   }
   QGraphicsView::mouseMoveEvent( mouseEvent_p );
}
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------