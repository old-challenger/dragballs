// ---------------------------------------------------------------------------------------------------------
#include <QDragBallsScene.h>
#include <QEdgeItem.h>
// ---------------------------------------------------------------------------------------------------------
#include <QGraphicsPixmapItem>
// ---------------------------------------------------------------------------------------------------------
void QDragBallsScene::setClicked( QDragBall * clickedItem_p )
{	clickingHit_m = true; }
// ---------------------------------------------------------------------------------------------------------
QEdgeItem * QDragBallsScene::findEdge(	QDragBall::vertexParent_class::edges_iterator begin_p,  
                                       QDragBall::vertexParent_class::edges_const_iterator end_p,
											      id_type endID_p ) const
{
	for( QDragBall::vertexParent_class::edges_iterator it = begin_p; end_p != it; ++it )
		if ( it->second.end().data().item()->id() == endID_p ) return it->second.data().item();
	return NULL;
}
// ---------------------------------------------------------------------------------------------------------
void QDragBallsScene::delBall( QDragBall * item_p )
{
	if ( NULL == item_p ) return ;
	// �������� ���� ������� ����� 
   QEdgeItem * edgeItem_l = NULL;
	while ( item_p->inEdgesEnd() != item_p->inEdgesBegin() )
	{
		edgeItem_l = item_p->inEdgesBegin()->second.data().item();
		item_p->inEdgesBegin()->second.begin().dellOutEdge( item_p->inEdgesBegin()->second.beginKey() );
		item_p->dellInEdge( item_p->inEdgesBegin()->first );
		edgeItem_l->hide( );
		removeItem( edgeItem_l );
	}
	while ( item_p->outEdgesEnd() != item_p->outEdgesBegin() )
	{
		edgeItem_l = item_p->outEdgesBegin()->second.data().item();
		item_p->outEdgesBegin()->second.end().dellInEdge( item_p->outEdgesBegin()->second.endKey() );
		item_p->dellOutEdge( item_p->outEdgesBegin()->first );
		edgeItem_l->hide( );
		removeItem( edgeItem_l );
	}
	if ( dragBallData::BALL_TYPE_OUR == item_p->type() ) ours_m.erase( item_p->id() );
   balls_m.erase( item_p->id() ); 
	item_p->hide();
	removeItem( item_p );
}
// ---------------------------------------------------------------------------------------------------------
void QDragBallsScene::mouseReleaseEvent ( QGraphicsSceneMouseEvent * mouseEvent_p ) 
{
   if ( clickingHit_m ) clickingHit_m = false;
   QGraphicsScene::mouseReleaseEvent( mouseEvent_p );
} 
// ---------------------------------------------------------------------------------------------------------
const QDragBall * QDragBallsScene::ball( id_type id_p ) const 
{	
	dragBalls_map::const_iterator found_l = balls_m.find( id_p ); 
	return balls_m.end() != found_l ? found_l->second.constData().item() : NULL;
}
// ---------------------------------------------------------------------------------------------------------
QEdgeItem * QDragBallsScene::arrow( id_type begin_p, id_type end_p )
{	
	dragBalls_map::iterator	begin_l = balls_m.find( begin_p );
	dragBalls_map::const_iterator end_l = balls_m.find( end_p );
	return	balls_m.end( ) == begin_l || balls_m.end( ) == end_l
				? NULL : findEdge(   begin_l->second.outEdgesBegin(),
									      begin_l->second.outEdgesEnd(), end_p );
}
// ---------------------------------------------------------------------------------------------------------
QDragBall * QDragBallsScene::addBall(	double x_p, double y_p, id_type id_p, 
											      int type_p, const QColor & color_cp, 
											      const QString & inText_cp, const QString & outText_cp, 
                                       const QFont & inFont_cp, const QFont & outFont_cp )
{
	QDragBall * item_l = NULL;
	dragBalls_map::iterator found_l = balls_m.lower_bound( id_p );
	if ( balls_m.end() == found_l || found_l->first != id_p )
	{
		item_l = new QDragBall(	id_p, x_p, y_p, ( dragBallData::ballsTypes ) type_p, color_cp, 
									   inText_cp, outText_cp, inFont_cp, outFont_cp );
      found_l = balls_m.insert(  found_l,
                                 dragBalls_pair( id_p, *( ( QDragBall::vertexParent_class * ) item_l ) ) );
      if ( dragBallData::BALL_TYPE_OUR == type_p ) ours_m.insert( id_p );
		addItem( item_l );
		item_l->show();
      item_l->update();
	}
	else item_l = found_l->second.data().item();
	return item_l;
}
// ---------------------------------------------------------------------------------------------------------
QEdgeItem * QDragBallsScene::addRelation(	
               id_type begin_p, id_type end_p, double length_p,
					const QColor & color_cp, const QString & text_cp, const QFont & font_cp,
					relationsEndingsTypes beginType_p, relationsEndingsTypes enmEndType_p )
{
	dragBalls_map::iterator	begin_l = balls_m.find( begin_p ),
						   		end_l = balls_m.find( end_p );
	if ( balls_m.end( ) == begin_l || balls_m.end( ) == end_l ) return NULL;
	// �������� �� ������������� ������ �����
	QEdgeItem * newEdge_l = findEdge(   begin_l->second.outEdgesBegin(), 
                                       begin_l->second.outEdgesEnd(), end_p );
	if ( NULL == newEdge_l )
		newEdge_l = findEdge(	end_l->second.outEdgesBegin(),
			   					   end_l->second.outEdgesEnd(), begin_p );
	if( NULL == newEdge_l )
	{
		newEdge_l = new QEdgeItem ( length_p, color_cp, text_cp, font_cp );
		begin_l->second.addOutEdge( *newEdge_l );
		end_l->second.addInEdge( *newEdge_l );
		addItem( newEdge_l );
		newEdge_l->show();
	}
	newEdge_l->update();
	return newEdge_l;
}
// ---------------------------------------------------------------------------------------------------------
QGraphicsPixmapItem * QDragBallsScene::addPicture (   double x_p, double y_p, id_type id_p, 
                                                      const QPixmap & picture_cp )
{
   QGraphicsPixmapItem * pixmap_l = NULL;
   pixmaps_map::const_iterator found_l = pixmaps_m.lower_bound( id_p );
   if ( pixmaps_m.end() == found_l || found_l->first != id_p )
   {
      pixmap_l = addPixmap( picture_cp );
      pixmap_l->setPos( x_p, y_p );
      pixmap_l->show();
      pixmap_l->update();
      pixmap_l->setZValue( Z_VALUE_PIXMAP );
      found_l = pixmaps_m.insert( found_l, pixmaps_pair( id_p, pixmap_l ) );    
   }
   else pixmap_l = found_l->second;
   return pixmap_l;
}
// ---------------------------------------------------------------------------------------------------------
void QDragBallsScene::placeItems( const std::set< int > * forTypes_p )
{
	bool  changed_l      = true,
         needUpdate_l   = false;
   static const unsigned int iterationsCounter_scl = 333;
   unsigned int iterationsCounter_l = 0;
	while ( changed_l && ++iterationsCounter_l < iterationsCounter_scl )
	{
		// ������� �������� ����� ��������� �����, ..
		for (	dragBalls_map::iterator it = balls_m.begin(); balls_m.end() != it; ++it )
			if(	NULL == forTypes_p 
            || forTypes_p->end() != forTypes_p->find( it->second.constData().item()->ballType() ) )
				it->second.data().item()->computeNewPosition( distanceFactor_m, maxPower_scm, 
               dragBallData::BALL_TYPE_OUR == it->second.constData().item()->ballType() 
                  ? QDragBall::PROCESSING_EDGES_OWN : QDragBall::PROCESSING_EDGES_ALIEN );
		// .. ����� "�������������" ���������������� ���� ��������
		changed_l = false;
		for (	dragBalls_map::iterator it = balls_m.begin(); balls_m.end() != it; ++it )
			if(   NULL == forTypes_p 
            || forTypes_p->end() != forTypes_p->find( it->second.constData().item()->ballType() ) )
         {  if ( it->second.data().item()->applyNewPosition() ) changed_l = true; }
	} // ������� ����������� �� ��� ��� ���� ������� �� ���������� �� �����
	update();
}
// ---------------------------------------------------------------------------------------------------------
bool QDragBallsScene::ballIsInOursRect ( id_type ballID_p ) const
{
   const QDragBall * ourBall_l = NULL;
   const QDragBall * checkingBall_l = ball( ballID_p );
   if ( NULL == checkingBall_l ) return false;
   //
   QPointF  topLeft_l      (  (std::numeric_limits< double >::min)(), 
      (std::numeric_limits< double >::min)() ),
      bottomRight_l  (  (std::numeric_limits< double >::max)(), 
      (std::numeric_limits< double >::max)() );
   for ( ids_set::const_iterator it = ours_m.begin(); it != ours_m.end(); ++it )
   {
      ourBall_l = ball( *it );
      assert( NULL != ourBall_l );
      topLeft_l = QPointF( std::max< qreal >( topLeft_l.x(), ourBall_l->x() ),
         std::max< qreal >( topLeft_l.y(), ourBall_l->y() ) );
      bottomRight_l = QPointF(   std::min< qreal >( bottomRight_l.x(), ourBall_l->x() ),
         std::min< qreal >( bottomRight_l.y(), ourBall_l->y() ) );
   }
   return QRectF( topLeft_l, bottomRight_l ).contains( checkingBall_l->pos() );
}
// ---------------------------------------------------------------------------------------------------------
void QDragBallsScene::keepBalls(	const ids_set & ids_p, const std::set< int > * forTypes_p )
{
   /* ������� ���� ������� � �������� ���� �� ���������� �� ��������� ids_p,
      �� ������������� �������� ����� forTypes_p.
      !!! �������� !!!
      ��� �������� ������ ������� delBall() ��������� ������ ������������� 
      ��������� (QDragBall *), ������������ ��������� ������������.
   */
   QDragBall * itemForDell_l = NULL;
	for (	dragBalls_map::iterator it = balls_m.begin();
			balls_m.end() != it; )
	{
		itemForDell_l = 
			ids_p.end() != ids_p.find( it->second.constData().item()->id() ) 
            ? NULL 
            : NULL == forTypes_p 
                  ? it->second.data().item() 
                  : forTypes_p->end() != forTypes_p->find( it->second.constData().item()->ballType() ) 
                        ? NULL : it->second.data().item();		
      /* !!! �������� !!!
         ����� ���������� �������� ++it � ������������� ���� ���� for() ������ 
         ��� ��� ������� delBall() ����� ������� ������� it �� ����������� balls_m.
      */
      ++it;
		delBall( itemForDell_l );
	}
}
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------

