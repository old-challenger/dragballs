// ---------------------------------------------------------------------------------------------------------
#include <QEdgeItem.h>
#include <QDragBallsScene.h>
// ---------------------------------------------------------------------------------------------------------
edgeItemData::edgeItemData(   QEdgeItem * graphicsItem_p, 
								      double length_p, const QColor & color_cp,
								      const QString & text_cp, const QFont & font_cp ) :
	beginPoint_m	( 0, 0 ),
	endPoint_m		( 0, 0 ),
	length_m	      ( length_p ),
	angle_m			( 0 ),
	lineColor_m		( color_cp ),
   graphicsItem_m	( graphicsItem_p ),
	textItem_m		( new QGraphicsTextItem( text_cp, static_cast< QGraphicsItem * >( graphicsItem_p ) ) )
{	textItem_m->setPos( 0, 0 ); }
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
bool QEdgeItem::invalidData()
{
	// �������� �� ������ ��������
	if ( constBegin().noData() || constEnd().noData() ) return true;
	// �������� �� ������ ���������
	if ( NULL == constBegin().constData().graphicsItem_m || NULL == constEnd().constData().graphicsItem_m ) 
		 return true;
	// ��� ���������
	return false;
}
// ---------------------------------------------------------------------------------------------------------
void QEdgeItem::setText( const QString & text_cp, const QFont * font_cp )
{
	if ( NULL == constData().textItem_m ) return ;
	if ( !text_cp.isEmpty() )
	{
      edgeParent_class::data().textItem_m->setPlainText( text_cp );
		edgeParent_class::data().textItem_m->show();
	}
	else edgeParent_class::data().textItem_m->hide();
	if ( NULL != font_cp ) edgeParent_class::data().textItem_m->setFont( *font_cp );
}
// ---------------------------------------------------------------------------------------------------------
void QEdgeItem::computeSelfPoints()
{
	if ( invalidData() ) return ;
	// ��������� �� ����������� ��������-����
	const QDragBall   * const beginGrphcs_cl  = constBegin().constData().graphicsItem_m,
						   * const endGrphcs_cl    = constEnd().constData().graphicsItem_m;
	// ����������� �����������
	const QLineF line_cl( beginGrphcs_cl->pos(), endGrphcs_cl->pos() );
	const qreal	legth_cl = line_cl.length();
	const QPointF direction_cl = ( 0 == legth_cl ) ? QPointF( 0, 0 ) : 
									         ( endGrphcs_cl->pos() - beginGrphcs_cl->pos() ) / legth_cl;
	// ��������� ��������, ��������� �� ������� ����
	const qreal factor_cl = 0.5 * constBegin().constData().ballSize_m;
	// ������ � ����� �������
	const QPointF	begin_cl	= beginGrphcs_cl->pos() + direction_cl * factor_cl,
	   				end_cl   = endGrphcs_cl->pos() - direction_cl * factor_cl;
	// �������� ��������������� �����
	const QPointF currentPos_cl = ( begin_cl + end_cl ) / 2;
	if ( pos() != currentPos_cl ) setPos( currentPos_cl );
	// ��������� �������� 
	edgeParent_class::data().beginPoint_m  = mapFromScene( begin_cl   );
	edgeParent_class::data().endPoint_m    = mapFromScene( end_cl     );
	//update( );
}
// ---------------------------------------------------------------------------------------------------------
const QPointF QEdgeItem::computeDelta(	unsigned int distanceFactor_p, 
											      double maxPower_p, const QLineF & line_cp ) const
{	
	const double	lineLenght_cl  = line_cp.length(),
					   deltaLength_cl = lineLenght_cl - constData().length_m;
                                    //legthByPower( distanceFactor_p, maxPower_p );
	//double deltaX_l = 1, deltaY_l = 1;
	//if ( 0 != lineLenght_cl )
	//	deltaX_l = line_cp.dx() / lineLenght_cl,
	//	deltaY_l = line_cp.dy() / lineLenght_cl;
	//else 
	//	deltaX_l = static_cast< qreal >( qrand() % 100 ) / 100,
	//	deltaY_l = sqrt( 1.0 - deltaX_l * deltaX_l );
   if ( 0 == lineLenght_cl )
   {
      const qreal randXdirection_cl = static_cast< qreal >( qrand() % 100 ) / 100;
      return randXdirection_cl
         * QPointF( randXdirection_cl, sqrt( 1.0 - randXdirection_cl * randXdirection_cl ) );
   }
   else
   {
      const QLineF unitVector_cl = line_cp.unitVector();
      return  QPointF( unitVector_cl.dx(), unitVector_cl.dy() ) * deltaLength_cl;
   }
}
// ---------------------------------------------------------------------------------------------------------
QRectF QEdgeItem::boundingRect() const
{	
	const QRectF result_cl( constData().beginPoint_m, constData().endPoint_m );
	return result_cl.normalized().adjusted( -7, -7, 7, 7 ); 
}
// ---------------------------------------------------------------------------------------------------------
QPainterPath QEdgeItem::shape() const
{
	QPainterPath qtPath_l;
	qtPath_l.addRect( QRectF( constData().beginPoint_m, constData().endPoint_m ) );
	return qtPath_l;
}
// ---------------------------------------------------------------------------------------------------------
void QEdgeItem::paint( QPainter * painter_p, const QStyleOptionGraphicsItem * option_p, QWidget * widget_p )
{
	if ( invalidData() ) return ;
	// ��������� �������
	painter_p->setPen( QPen( constData().lineColor_m, 1, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
	painter_p->drawLine( constData().beginPoint_m, constData().endPoint_m );
	// ����� ��� ����c����� ���� �������
	const QLineF line_cl( constData().endPoint_m, constData().beginPoint_m );
	//qreal angle_l = atan2( qtLine_l.dy(), qtLine_l.dx() );

	/* �����������
	QPolygonF qtTriangle_l;
	qtTriangle_l.append( constEdge().qtEndPoint );
	qtTriangle_l.append( QPointF(	constEdge().qtEndPoint.x() + cos( qrAngle_l += pi_div6() ) * 13,	
									constEdge().qtEndPoint.y() + sin( qrAngle_l ) * 13 ) );
	qtTriangle_l.append( QPointF(	constEdge().qtEndPoint.x() + cos( qrAngle_l -= pi_div3() ) * 13,	
									constEdge().qtEndPoint.y() + sin( qrAngle_l ) * 13 ) );
	pPainter_p->setBrush( Qt::black );
	pPainter_p->drawPolygon( qtTriangle_l ); /**/

	// ��������� ������ 
	if ( NULL == constData().textItem_m ) return ;
	const QString text_cl = constData().textItem_m->toPlainText();
	if ( text_cl.isEmpty() ) return ;
	QFontMetrics qtFontMetrics_l( constData().textItem_m->font() );
	if ( line_cl.length() < qtFontMetrics_l.width( text_cl ) * 1.1 ) 
      edgeParent_class::data().textItem_m->hide(); 
	else //if ( edge().dAngle != qrAngle_l )
	{
		if ( !edgeParent_class::data().textItem_m->isVisible() ) edgeParent_class::data().textItem_m->show();
		/*qreal qrDeltaDegrees_l = ( constEdge().dAngle - qrAngle_l ) * uiPiInDegrees_cs;
		//if ( utl::abs( qrAngle_l ) > pi_div2() ) qrDeltaDegrees_l += uiPiInDegrees_cs;
		edge().pTextItem->rotate( qrDeltaDegrees_l );
		edge().dAngle = qrAngle_l;*/
	}
}
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------