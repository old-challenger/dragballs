// ---------------------------------------------------------------------------------------------------------
#include <QDragBallsScene.h>
#include <QBallItem.h>
// ---------------------------------------------------------------------------------------------------------
#include <assert.h>
// ---------------------------------------------------------------------------------------------------------
dragBallData::dragBallData(	QDragBall * graphicsItem_p, id_type id_p, 
								      ballsTypes type_p, const QColor & color_cp, 
								      const QString & inText_cp, const QFont & inFont_cp,	
								      const QString & outText_cp, const QFont & outFont_cp ) :	
   selected_m		( false     ),
   ballSize_m		( 33        ), 
   id_m			   ( id_p      ),
   type_m			( type_p    ),
   color_m			( color_cp  ),
   inText_m 		( inText_cp ),
   inFont_m		   ( inFont_cp ),
   newPosition_m	( 0, 0 ),
   graphicsItem_m	( graphicsItem_p ),
   outTextItem_m	( new QGraphicsTextItem( outText_cp, static_cast< QGraphicsItem * >( graphicsItem_p ) ) )
{	}
// ---------------------------------------------------------------------------------------------------------
dragBallData::dragBallData( const dragBallData & strctEtalon_cp ) :
   selected_m		( strctEtalon_cp.selected_m      ),
   ballSize_m		( strctEtalon_cp.ballSize_m		),
   id_m			   ( strctEtalon_cp.id_m			   ),
   type_m			( strctEtalon_cp.type_m			   ),
   color_m			( strctEtalon_cp.color_m			),
   inText_m 		( strctEtalon_cp.inText_m 		   ),
   inFont_m		   ( strctEtalon_cp.inFont_m		   ),
   newPosition_m	( strctEtalon_cp.newPosition_m   ),
   graphicsItem_m	( strctEtalon_cp.graphicsItem_m	),
   outTextItem_m	( strctEtalon_cp.outTextItem_m	)
{	}
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
void QDragBall::computeDelta( const vertexParent_class & incidentVertex_cp, unsigned int distanceFactor_p, 
                              double maxPower_p, processingEdges processingEdges_p, 
                              QEdgeItem & incidentEdge_p, unsigned int & notNullCounter_p, 
                              QPointF & delta_p )
{
   /* �������� ��� ����� incidentEdge_p �� ����������� � ��� ������ ���� 
      1. ����� �������� ������
      2. �� ��������� ���� "������������ ����� �� ����� ����� ������", � ������� 
      incidentVertex_cp ������ ���� � �������.
      3. �� ��������� �� ���� "������������ ����� � ������ ������ ������", � ������� 
      incidentVertex_cp ������� ���� � �������.
   */
   if (     incidentVertex_cp.constData() == constData() 
         || (     !( PROCESSING_EDGES_OWN & processingEdges_p ) 
               && incidentVertex_cp.constData().type_m == constData().type_m )
         || (     !( PROCESSING_EDGES_ALIEN & processingEdges_p ) 
               && incidentVertex_cp.constData().type_m != constData().type_m ) ) 
   {  return ; }
   //
   const QLineF line_l(	QPointF( 0, 0 ), mapFromItem( incidentVertex_cp.constData().item(), 0, 0 ) ); 
   delta_p += incidentEdge_p.computeDelta( distanceFactor_p, maxPower_p, line_l ) ;
   ++notNullCounter_p;
}
// ---------------------------------------------------------------------------------------------------------
void QDragBall::mousePressEvent ( QGraphicsSceneMouseEvent * mouseEvent_p ) 
{
   QDragBallsScene * scene_l = dynamic_cast< QDragBallsScene * >( scene() );
   assert( NULL != scene_l );
   // �������� �� ����������� ��������������
   checkMovable ( scene_l->editionModeEnabled() );
	if ( scene_l->editionModeEnabled() && Qt::LeftButton == mouseEvent_p->button() )
	{
      setSelected(); 
      update();
		scene_l->setClicked( this );
		QGraphicsItem::mousePressEvent( mouseEvent_p );
	}
	else mouseEvent_p->accept();
}
// ---------------------------------------------------------------------------------------------------------
void QDragBall::mouseReleaseEvent ( QGraphicsSceneMouseEvent * mouseEvent_p )
{
   QDragBallsScene * scene_l = dynamic_cast< QDragBallsScene * >( scene() );
   assert( NULL != scene_l );
   //
   if ( scene_l->editionModeEnabled() && Qt::LeftButton == mouseEvent_p->button() )
   {
      setSelected( false ); 
      update();
      QGraphicsItem::mouseReleaseEvent( mouseEvent_p );
   }
   else mouseEvent_p->accept();
}
// ---------------------------------------------------------------------------------------------------------
QVariant QDragBall::itemChange ( QGraphicsItem::GraphicsItemChange change_p, const QVariant & value_cp ) 
{
	if ( QGraphicsItem::ItemPositionHasChanged == change_p )
	{
		for (	edges_iterator it = inEdgesBegin(); inEdgesEnd() != it; ++it )
		   it->second.data().item()->computeSelfPoints();
		for (	edges_iterator it = outEdgesBegin(); outEdgesEnd() != it; ++it )
			it->second.data().item()->computeSelfPoints();
	}
	return QGraphicsItem::itemChange( change_p, value_cp );
}
// ---------------------------------------------------------------------------------------------------------
QDragBall::QDragBall(   dragBallData::id_type id_p, double x_p, double y_p, 
							   dragBallData::ballsTypes type_p, const QColor & color_cp, 
							   const QString  & inText_cp, const QString & outText_cp, 
                        const QFont    & inFont_cp, const QFont   & outFont_cp ) :
	QGraphicsItem( NULL ),
	vertexParent_class( dragBallData(   this, id_p, type_p, color_cp, 
                                       inText_cp, inFont_cp, outText_cp, outFont_cp ) )
{
	setPos( x_p, y_p );
	setZValue( Z_VALUE_ITEM );
	if( outText_cp.isEmpty() ) 
      vertexParent_class::data().outTextItem_m->hide();
   else
   {
      const int outTextPosition_cl = vertexParent_class::constData().ballSize_m / 3 + 1;
      vertexParent_class::data().outTextItem_m->setZValue( Z_VALUE_TEXT );
      vertexParent_class::data().outTextItem_m->setPos( outTextPosition_cl, outTextPosition_cl ); 
   }
}
// ---------------------------------------------------------------------------------------------------------
void QDragBall::setOutText( const QString & text_cp, const QFont * font_cp )
{
	if ( NULL == constData().outTextItem_m ) return ;
	if ( !text_cp.isEmpty() )
	{
		vertexParent_class::data().outTextItem_m->setPlainText( text_cp );
		vertexParent_class::data().outTextItem_m->show();
	}
	else vertexParent_class::data().outTextItem_m->hide();
	if ( NULL != font_cp ) vertexParent_class::data().outTextItem_m->setFont( *font_cp );
}
// ---------------------------------------------------------------------------------------------------------
void QDragBall::paint( QPainter * painter_p, const QStyleOptionGraphicsItem * option_p, QWidget * widget_p )
{
	QFontMetrics fontMetrics_l( constData().inFont_m );
	//QString qstrInText_l = QString::number( static_cast< unsigned int>( constVertex().szKey ) );
	// ������ ��������� ������ ������ ���� � ���������� ���������� ������ ����
	const int   txtWidth_cl	   = fontMetrics_l.width( constData().inText_m ),
		         minBallSize_cl = txtWidth_cl  ? std::max<int>( fontMetrics_l.height(), txtWidth_cl ) * 1.25 
                                             : 10;
	// ���� ������ ���� ������� ��� - ����������� ���
	if ( minBallSize_cl > ballSize() ) setBallSize( minBallSize_cl );/**/
	// ������ ��������� ��������� � �������� Item'�
	const int   half_cl     = constData().ballSize_m / 2,		// �������� �� ������� ����
		         oneTenth_cl	= constData().ballSize_m / 10,   // ������� ����� �������
		         textX_cl		= - txtWidth_cl / 2,				   // �������� �� ������� ������ ������ ����
		         textY_cl		= - fontMetrics_l.height() / 2,	// �������� �� ������ ������ ������ ����
		         shadow_cl	= constData().ballSize_m * shadowOffset() - half_cl;
	// ����������� ������� ����
	QRadialGradient gradient_l( - oneTenth_cl, - oneTenth_cl, half_cl );
	// �����
	QColor	light_l     = constData().color_m.light( 177 ), // Qt::green,		
   			textColor_l = Qt::white;
	// ��������� ������ � ����������� �� ��������� Item'�
	if ( constData().selected_m ) 
	{
		gradient_l.setCenter( oneTenth_cl, oneTenth_cl );
		gradient_l.setFocalPoint( oneTenth_cl, oneTenth_cl );
		gradient_l.setColorAt( 1, light_l.light( 120 ) );
		gradient_l.setColorAt( 0, constData().color_m.light( 120 ) );
		textColor_l = Qt::black;
	}
	else 
	{
		gradient_l.setColorAt( 0, light_l );
		gradient_l.setColorAt( 1, constData().color_m );
	}
	// ��������� ����
	QColor shadowColor_l = Qt::darkGray;
	shadowColor_l.setAlpha( 177 );
	painter_p->setPen	( Qt::NoPen );
	painter_p->setBrush( shadowColor_l );
	painter_p->drawEllipse( shadow_cl, shadow_cl, constData().ballSize_m, constData().ballSize_m );
	// ��������� ����
	painter_p->setBrush( gradient_l );
	painter_p->setPen( QPen( Qt::black, 0 ) );
	painter_p->drawEllipse(	-half_cl, -half_cl, constData().ballSize_m, constData().ballSize_m );
	// ��������� ������ ������ ����
	painter_p->setPen( textColor_l );
	painter_p->setFont( constData().inFont_m );
   painter_p->setBrush( Qt::transparent );
   painter_p->drawText(	textX_cl, textY_cl, txtWidth_cl, fontMetrics_l.height(), 
							   Qt::AlignCenter, constData().inText_m );/**/
}
// ---------------------------------------------------------------------------------------------------------
void QDragBall::computeNewPosition( unsigned int distanceFactor_p, double maxPower_p, 
                                    processingEdges processingEdges_p )
{
	unsigned int notNullCounter_l = 0;
	QPointF delta_l( 0, 0 );
   //
	for (	vertexParent_class::edges_iterator it = inEdgesBegin(); inEdgesEnd() != it; ++it )
      computeDelta(  it->second.begin(), distanceFactor_p, maxPower_p, processingEdges_p, 
                     *it->second.data().item(), notNullCounter_l, delta_l );
	//{
	//	if ( it->second.begin().constData() == constData() ) continue;
	//	const QLineF line_l(	QPointF( 0, 0 ), mapFromItem( it->second.begin().constData().item(), 0, 0 ) ); 
	//	delta_l += it->second.data().item()->computeDelta( distanceFactor_p, maxPower_p, line_l ) ;
	//	++notNullCouter_l;
	//}
   //
	for (	vertexParent_class::edges_iterator it = outEdgesBegin(); outEdgesEnd() != it; ++it )
      computeDelta(  it->second.end(), distanceFactor_p, maxPower_p, processingEdges_p,
                     *it->second.data().item(), notNullCounter_l, delta_l );
	//{
	//	if ( it->second.end().constData() == constData() ) continue;
	//	const QLineF line_l( QPointF( 0, 0 ), mapFromItem( it->second.end().constData().item(), 0, 0 ) ); 
	//	delta_l += it->second.data().item()->computeDelta( distanceFactor_p, maxPower_p, line_l );
	//	++notNullCouter_l;
	//}
	if ( notNullCounter_l ) delta_l /= notNullCounter_l << 1;
	if ( 0.1 > utl::abs( delta_l.x() ) && 0.1 > utl::abs( delta_l.y() ) ) delta_l = QPointF( 0, 0 );
	vertexParent_class::data().newPosition_m = pos() + delta_l;
}
// ---------------------------------------------------------------------------------------------------------
QRectF QDragBall::boundingRect() const
{
	const int   half_cl = - constData().ballSize_m / 2,
				   sizeWithShadow_cl = constData().ballSize_m * 1.17,
	            adjust_cl = 2;
	return QRectF(	half_cl - adjust_cl, half_cl - adjust_cl,
					   sizeWithShadow_cl + adjust_cl, sizeWithShadow_cl + adjust_cl );
}
// ---------------------------------------------------------------------------------------------------------
QPainterPath QDragBall::shape() const
{
	const int	half_cl = - constData().ballSize_m / 2,
				   sizeWithShadow_cl = constData().ballSize_m;
	QPainterPath path_l;
	path_l.addEllipse( half_cl, half_cl, sizeWithShadow_cl, sizeWithShadow_cl );
	return path_l;
}
// ---------------------------------------------------------------------------------------------------------
QDragBall::~QDragBall()
{
	vertexParent_class::data().outTextItem_m    = NULL;
	vertexParent_class::data().graphicsItem_m   = NULL;
}
// ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------
