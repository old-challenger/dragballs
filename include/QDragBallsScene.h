// ---------------------------------------------------------------------------------------------------------
#ifndef QT_DRAG_BALLS_SCENE_H_MRV
#define QT_DRAG_BALLS_SCENE_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!	\file		dragBallsScene.h
	   \author	MRV
	   \date		05.11.2009
	   \version	0.5
	   \brief	����� ��� ����������� ��������� ������� �������� �� ���������


	   ���������, �����������, ����������.
	   v.0.5:
	   <ol>
		   <li> [created]	
	   </ol>

	   ��������, ���������, ��������:
	   <ol>
		   <li> ��� ����� �������� \n
			   � ���
	   </ol>
*/
// ---------------------------------------------------------------------------------------------------------
#include <QBallItem.h>
// ---------------------------------------------------------------------------------------------------------
#include <QPixmap>
#include <QGraphicsScene>
// ---------------------------------------------------------------------------------------------------------
#include <cstdlib>
#include <limits>
#include <vector>
#include <cmath>
#include <ctime>
#include <map>
#include <set>
// ---------------------------------------------------------------------------------------------------------
#include <assert.h>
// ---------------------------------------------------------------------------------------------------------
class QGraphicsPixmapItem;
class edgeItemData;
class QEdgeItem;
// ---------------------------------------------------------------------------------------------------------
typedef unsigned __int64 uint64_t;
// ---------------------------------------------------------------------------------------------------------

/*! \brief	�����-�����

*/
class QDragBallsScene : public QGraphicsScene
{
friend class QDragBall;
public:
   enum relationsEndingsTypes
   {
      ENDINGS_TYPE_NOTHING	   = 0,
      ENDINGS_TYPE_CIRCLE		= 1,
      ENDINGS_TYPE_SQUARE		= 2,
      ENDINGS_TYPE_ARROW		= 3
   };
   // ------------------------------------------------------------------------------------------------------
   typedef dragBallData::id_type id_type;
   typedef std::set< id_type >   ids_set;
   typedef std::map< id_type, QGraphicsPixmapItem * >    pixmaps_map;
   typedef std::pair< id_type, QGraphicsPixmapItem * >   pixmaps_pair;
   typedef std::map< id_type, QDragBall::vertexParent_class >  dragBalls_map;
   typedef std::pair< id_type, QDragBall::vertexParent_class >	dragBalls_pair;
   // ------------------------------------------------------------------------------------------------------
private:
   enum { Z_VALUE_PIXMAP = 1 };
   // ------------------------------------------------------------------------------------------------------
	static const unsigned int maxPower_scm = 100;
   // ------------------------------------------------------------------------------------------------------
	bool			   clickingHit_m,
                  editionModeEnabled_m; 
	unsigned int	distanceFactor_m;
   pixmaps_map    pixmaps_m;
	dragBalls_map	balls_m;
   ids_set        ours_m;

   std::set< QDragBall * > selectedItems_m;
   // ------------------------------------------------------------------------------------------------------

	static QColor colorByNumber( unsigned int number_p )
	{	
      return QColor(	static_cast< unsigned char >( number_p ), 
                     static_cast< unsigned char >( number_p >> 8 ), 
						   static_cast< unsigned char >( number_p >> 16 ) ); 
   }
   // ------------------------------------------------------------------------------------------------------

	static QString lastByteAsString( unsigned int number_p )
	{	return QString::number( number_p, 16 ).right( 2 ); }
   // ------------------------------------------------------------------------------------------------------

	void setClicked( QDragBall * selectedItem_p );
   // ------------------------------------------------------------------------------------------------------

   inline QEdgeItem * findEdge(	QDragBall::vertexParent_class::edges_iterator itBegin_p,  
                                 QDragBall::vertexParent_class::edges_const_iterator itFirstEnd_p,
									      id_type endID_p ) const;
   // ------------------------------------------------------------------------------------------------------

	void delBall( QDragBall * item_p );
   // ------------------------------------------------------------------------------------------------------
protected:

	virtual void mouseReleaseEvent ( QGraphicsSceneMouseEvent * mouseEvent_p );
   // ------------------------------------------------------------------------------------------------------
public:

	QDragBallsScene( QObject * parent_p = NULL ) :
		QGraphicsScene	      ( parent_p ),
      clickingHit_m        ( false ),
      editionModeEnabled_m ( false ),
		distanceFactor_m     ( 7 ),
      pixmaps_m            (),
      balls_m              (),
      ours_m               (),
      selectedItems_m      ()

	{	srand( time( NULL ) ); }
   // ------------------------------------------------------------------------------------------------------

	// qt-�������� � ����� �����
   unsigned int distanceFactor () const { return distanceFactor_m; }
   // ------------------------------------------------------------------------------------------------------

   bool clickingHit () const { return clickingHit_m; } 
   // ------------------------------------------------------------------------------------------------------

   bool editionModeEnabled () const { return editionModeEnabled_m; }
   // ------------------------------------------------------------------------------------------------------

   void setEditionModeEnabled ( bool value_p ) { editionModeEnabled_m = value_p; }
	// ------------------------------------------------------------------------------------------------------
   
   // ���������� ����� �������� � ����� �����
   void setDistanceFactor ( unsigned int value_p ) { distanceFactor_m = value_p; }
   // ------------------------------------------------------------------------------------------------------

   double metersToDistance ( double meters_p ) const { return distanceFactor_m * meters_p; }
   // ------------------------------------------------------------------------------------------------------

   double distanceToMeters ( double distance_p ) const { return distance_p / distanceFactor_m; }
   // ------------------------------------------------------------------------------------------------------
		
   double powerToDistance ( double power_p ) const
   {
      /* 1. ��������� ������� ��������������� �������� ����������� 
      double dNormalizedPower_l = 
      std::max<double>( 1.0, std::min<double>( constEdge().dSignalPower, dMaxPower_p ) );
      return pow( 1.0 / dNormalizedPower_l, 0.5 ) * 1000; /**/
      /* 2. ��������������� ����������� ���������� �� ������������� ��������� */
      static const double logFactor_scl = 13;
      return - log( std::max< double >( 1, power_p ) / maxPower_scm ) * logFactor_scl * distanceFactor_m; 
      /**/ 
      /* 3. ���������������� ����������� ���������� �� ��������
      return ; /**/
      /* 4. ��������� ����������� 
      return pow( dMaxPower_p - constEdge().dSignalPower, 2 ) * 0.1; /**/
   }
   // ------------------------------------------------------------------------------------------------------

   double distanceToPower ( double distance_p ) const
   {  return maxPower_scm / exp( distance_p / distanceFactor_m ); }
   // ------------------------------------------------------------------------------------------------------
   
   // �������� ������ � ���������� ����
   const QDragBall * ball ( id_type id_p ) const;
   // ------------------------------------------------------------------------------------------------------

   QDragBall * ball ( id_type id_p )
   {  return const_cast< QDragBall * >( static_cast< const QDragBallsScene * >( this )->ball( id_p ) ); }
   // ------------------------------------------------------------------------------------------------------


	// �������� ������ � ���������� �������
	QEdgeItem * arrow ( id_type begin_p, id_type end_p );
   // ------------------------------------------------------------------------------------------------------

   QGraphicsPixmapItem * picture ( id_type id_p )
   {
      pixmaps_map::iterator found_l = pixmaps_m.find( id_p );
      return pixmaps_m.end() != found_l ? found_l->second : NULL;
   }
   // ------------------------------------------------------------------------------------------------------

   // �������� ���
	QDragBall * addBall (	double x_p, double y_p, id_type id_p, 
							      int type_p, const QColor & color_cp = Qt::darkGreen, 
							      const QString & inText_cp = QString(), 
                           const QString & outText_cp = QString(), 
                           const QFont & inFont_cp = QFont( "Times" ), 
                           const QFont & outFont_p = QFont( "Times" ) );
   // ------------------------------------------------------------------------------------------------------

	// �������� �����
	QEdgeItem * addRelation (  id_type begin_p, id_type end_p, double length_p, 
                              const QColor & color_p = Qt::black,
		 						      const QString & text_p = QString(), const QFont & font_p = QFont( "Times" ),
								      relationsEndingsTypes beginType_p = ENDINGS_TYPE_NOTHING,
								      relationsEndingsTypes endType_p = ENDINGS_TYPE_NOTHING );
   // ------------------------------------------------------------------------------------------------------

   QGraphicsPixmapItem * addPicture (  double x_p, double y_p, id_type id_p, 
                                       const QPixmap & picture_cp );
   // ------------------------------------------------------------------------------------------------------

   void clearSelection ()
   {
      for ( std::set< QDragBall * >::iterator it = selectedItems_m.begin();
            selectedItems_m.end() != it; ++it )
      {
         (*it)->setSelected( false );
         (*it)->update();
      }
      selectedItems_m.clear();
   }
   // ------------------------------------------------------------------------------------------------------

	// �������� ��������
	void selectBalls ( const std::vector< id_type > & ids_cp ) 
   { 
      assert( !"Function QDragBallScene::selectBalls() is not realized.\n"
               "Ask developer about this problem." );
   }
   // ------------------------------------------------------------------------------------------------------

	// ���������� ��������
	void placeItems ( const std::set< int > * types_cp = NULL ); 
   // ------------------------------------------------------------------------------------------------------

	// ������� ���������� ������� (���� � ������������ �����)
	void dellSelectedItems () 
   { 
      assert( !"Function QDragBallScene::dellSelectedItems() is not realized.\n"
               "Ask developer about this problem." );
   }
	// ------------------------------------------------------------------------------------------------------

	/*! \brief ������� ����� � �������� ���������������
   */
	void delBall ( id_type id_p ) { delBall( ball( id_p ) ); }
   // ------------------------------------------------------------------------------------------------------

	/*! ������� ������� � ���������� ���������������� (������ �� �������� - ���������� � ������������)
   */
	void delBalls ( const std::vector< id_type > & ids_p ) 
   {  
      assert( !"Function QDragBallScene::delBalls() is not realized.\n"
               "Ask developer about this problem." );
   }
   // ------------------------------------------------------------------------------------------------------

   void delPicture ( id_type id_p )
   {
      pixmaps_map::const_iterator found_cl = pixmaps_m.find( id_p );
      if ( pixmaps_m.end() != found_cl  )
      {
         found_cl->second->hide();
         removeItem( found_cl->second );
         pixmaps_m.erase( found_cl ); 
      }
   }
   // ------------------------------------------------------------------------------------------------------

   bool ballIsInOursRect ( id_type ballID_p ) const;
   // ------------------------------------------------------------------------------------------------------

	// ������� �������� ������������� � ���������, ��������� �������
	void keepBalls( const ids_set & ids_p, const std::set< int > * forTypes_p = NULL );
   // ------------------------------------------------------------------------------------------------------
}; // class QDragBallsScene
// ---------------------------------------------------------------------------------------------------------
#endif // QT_DRAG_BALLS_SCENE_H_MRV
// ---------------------------------------------------------------------------------------------------------
