// ---------------------------------------------------------------------------------------------------------
#ifndef QT_DRAG_BALL_ITEM_H_MRV
#define QT_DRAG_BALL_ITEM_H_MRV
// ---------------------------------------------------------------------------------------------------------
#include <QEdgeItem.h>
// ---------------------------------------------------------------------------------------------------------
#include <graphD.h>
// ---------------------------------------------------------------------------------------------------------
#include <QObject>
#include <QVariant>
#include <QPainter>
#include <QFontMetrics>
#include <QLatin1String>
#include <QGraphicsItem>
#include <QGraphicsTextItem>
#include <QGraphicsSceneMouseEvent>
// ---------------------------------------------------------------------------------------------------------
class QVariant;
// ---------------------------------------------------------------------------------------------------------
class QDragBall;
class edgeItemData;
// ---------------------------------------------------------------------------------------------------------

class dragBallData 
{
friend class QDragBall;
friend class QEdgeItem;
public:
   // ���� �����
	enum ballsTypes
	{
		BALL_TYPE_UNKNOWN	= 0,  ///< �� ��������
		BALL_TYPE_OUR		= 1,  ///< ���
		BALL_TYPE_FOREIGN	= 2   ///< �� ���
	};
   // ------------------------------------------------------------------------------------------------------
   typedef QString id_type;
   // ------------------------------------------------------------------------------------------------------
private:
	bool			selected_m;
	int			ballSize_m;
	id_type	   id_m;
	ballsTypes	type_m;

	QColor		color_m;
	QString		inText_m;
	QFont			inFont_m;
	QPointF		newPosition_m;
	
   QDragBall * graphicsItem_m;
   QGraphicsTextItem	* outTextItem_m;
   // ------------------------------------------------------------------------------------------------------
public:

	dragBallData (	QDragBall * graphicsItem_p = NULL, id_type id_p = QString(), 
					   ballsTypes type_p = BALL_TYPE_UNKNOWN, const QColor& color_p = Qt::darkGreen, 
					   const QString & inText_cp = "", const QFont & inFont_cp = QFont( "Times" ),	
					   const QString & outText_cp = "", const QFont & outFont_cp = QFont( "Times" ) );	
   // ------------------------------------------------------------------------------------------------------
	
	dragBallData ( const dragBallData & strctEtalon_�p );
   // ------------------------------------------------------------------------------------------------------

   ~dragBallData ()
   {	
      if ( NULL != outTextItem_m && NULL == outTextItem_m->parentItem() && NULL == outTextItem_m->scene() ) 
         delete outTextItem_m; 
   }
   // ------------------------------------------------------------------------------------------------------

   const QDragBall * item () const { return graphicsItem_m; }
   // ------------------------------------------------------------------------------------------------------

   QDragBall * item () { return graphicsItem_m; }
   // ------------------------------------------------------------------------------------------------------
		
   bool operator ==( const dragBallData & strctEtalon_cp ) const { return strctEtalon_cp.id_m == id_m; }
   // ------------------------------------------------------------------------------------------------------
}; // class dragBallData 
// ---------------------------------------------------------------------------------------------------------
// typedef utl::vertex< sDragBallData, sEdgeItemData > VertexParentClass_t;
// ---------------------------------------------------------------------------------------------------------

class QDragBall : public QGraphicsItem, public utl::vertex< dragBallData, edgeItemData >
{
public:
   enum { Type = UserType + 13 };
   // ------------------------------------------------------------------------------------------------------

   enum processingEdges
   {
      PROCESSING_EDGES_OWN    = 0x1,   ///< ������������ ����� �� ����� ����� ������
      PROCESSING_EDGES_ALIEN  = 0x2,   ///< 
      PROCESSING_EDGES_ALL    = PROCESSING_EDGES_OWN | PROCESSING_EDGES_ALIEN
   };
   // ------------------------------------------------------------------------------------------------------
   typedef utl::vertex< dragBallData, edgeItemData > vertexParent_class;
   // ------------------------------------------------------------------------------------------------------
private:
	enum zValues
	{
      Z_VALUE_TEXT		= 7,
		Z_VALUE_ITEM		= 11,
		Z_VALUE_SELECTED	= 13,
	};
   // ------------------------------------------------------------------------------------------------------

	static double shadowOffset() { return 0.13; }
   // ------------------------------------------------------------------------------------------------------

   void computeDelta (  const vertexParent_class & incidentVertex_cp, unsigned int distanceFactor_p, 
                        double maxPower_p, processingEdges processingEdges_p, 
                        QEdgeItem & incidentEdge_p, unsigned int & notNullCounter_p, QPointF & delta_p );
   // ------------------------------------------------------------------------------------------------------

   void checkMovable ( bool value_p )
   {
      bool oldValue_l = static_cast< bool >( flags() & ItemIsMovable );
      if ( value_p != oldValue_l ) setFlag( ItemIsMovable, value_p );
   }
   // ------------------------------------------------------------------------------------------------------
protected:

   virtual void mousePressEvent ( QGraphicsSceneMouseEvent * mouseEvent_p );
   // ------------------------------------------------------------------------------------------------------

   virtual void mouseReleaseEvent ( QGraphicsSceneMouseEvent * mouseEvent_p );
   // ------------------------------------------------------------------------------------------------------

	virtual QVariant itemChange ( enum QGraphicsItem::GraphicsItemChange �hange_p, 
                                 const QVariant & value_cp );
   // ------------------------------------------------------------------------------------------------------
public:

	QDragBall(	dragBallData::id_type id_p, double x_p, double y_p, 
			      dragBallData::ballsTypes type_p = dragBallData::BALL_TYPE_OUR, 
					const QColor & color_p = Qt::darkGreen, 
					const QString  & inText_cp    = QString(), 
               const QString  & outText_cp   = QString(), 
               const QFont    & inFont_cp    = QFont( "Times" ),
               const QFont    & outFont_cp   = QFont( "Times" ) );
   // ------------------------------------------------------------------------------------------------------

   ~QDragBall();
   // ------------------------------------------------------------------------------------------------------

   int type () const { return Type; }
   // ------------------------------------------------------------------------------------------------------

   bool isSelected () const {	return vertexParent_class::constData().selected_m; }
   // ------------------------------------------------------------------------------------------------------

	int ballSize () const {	return constData().ballSize_m; }
   // ------------------------------------------------------------------------------------------------------

	dragBallData::id_type id () const {	return constData().id_m; }
   // ------------------------------------------------------------------------------------------------------

	dragBallData::ballsTypes ballType () const { return constData().type_m; }
   // ------------------------------------------------------------------------------------------------------

	const QString & inText () const {	return constData().inText_m; }
   // ------------------------------------------------------------------------------------------------------

	const QString outText () const
	{	
      return NULL != constData().outTextItem_m  ? constData().outTextItem_m->toPlainText() 
                                                : QLatin1String( "" ); 
   }
   // ------------------------------------------------------------------------------------------------------

	void setSelected ( bool value_p = true )
   {	setZValue( ( vertexParent_class::data().selected_m = value_p ) ? Z_VALUE_SELECTED : Z_VALUE_ITEM ); }
   // ------------------------------------------------------------------------------------------------------

	void setBallSize ( int value_p )
	{	
		vertexParent_class::data().ballSize_m = value_p;
		if ( NULL != constData().outTextItem_m )
			vertexParent_class::data().outTextItem_m->setPos( 
            - constData().outTextItem_m->boundingRect().width() / 2, 
				constData().ballSize_m * ( 1 + shadowOffset() ) / 2 + 1 );
	}
   // ------------------------------------------------------------------------------------------------------

	void setColor ( const QColor & color_cp ) { vertexParent_class::data().color_m = color_cp; }
   // ------------------------------------------------------------------------------------------------------
	
   void setBallType ( dragBallData::ballsTypes type_p ) { vertexParent_class::data().type_m = type_p; }
   // ------------------------------------------------------------------------------------------------------

	void setInText ( const QString & text_cp, const QFont * font_cp = NULL )
	{	
      vertexParent_class::data().inText_m = text_cp; 
      if ( NULL != font_cp ) vertexParent_class::data().inFont_m = *font_cp; 
   }
   // ------------------------------------------------------------------------------------------------------

	void setOutText ( const QString & text_cp, const QFont * font_cp = NULL );
   // ------------------------------------------------------------------------------------------------------

	void computeNewPosition (  unsigned int distanceFactor_p, double maxPower_p, 
                              processingEdges processingEdges_p );
   // ------------------------------------------------------------------------------------------------------

	bool applyNewPosition ()
	{	
		if ( pos() == constData().newPosition_m ) return false;
		setPos( constData().newPosition_m ); 
		return true;
	}
   // ------------------------------------------------------------------------------------------------------

	QRectF boundingRect () const;
   // ------------------------------------------------------------------------------------------------------

	QPainterPath shape () const;
   // ------------------------------------------------------------------------------------------------------

	void paint ( QPainter * painter_p, const QStyleOptionGraphicsItem * option_p, QWidget * widget_p );
   // ------------------------------------------------------------------------------------------------------
}; // class QDragBall
// ---------------------------------------------------------------------------------------------------------
#endif // QT_DRAG_BALL_ITEM_H_MRV
// ---------------------------------------------------------------------------------------------------------
