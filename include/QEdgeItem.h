// ---------------------------------------------------------------------------------------------------------
#ifndef QT_EDGE_ITEM_H_MRV
#define QT_EDGE_ITEM_H_MRV
// ---------------------------------------------------------------------------------------------------------
#include <QBallItem.h>
// ---------------------------------------------------------------------------------------------------------
#include <graphD.h>
// ---------------------------------------------------------------------------------------------------------
#include <QGraphicsItem>
#include <QPainterPath>
#include <QString>
#include <QFont>
// ---------------------------------------------------------------------------------------------------------
#include <cmath>
// ---------------------------------------------------------------------------------------------------------
class dragBallData;
class QEdgeItem;
class QDragBall;
// ---------------------------------------------------------------------------------------------------------

class edgeItemData
{
friend class QEdgeItem;
friend class QDragBall;
friend class QDragBallsScene;
private:
	QPointF  beginPoint_m,
				endPoint_m;

	double   length_m,
				angle_m;

	QColor   lineColor_m;

   QEdgeItem * graphicsItem_m;
	QGraphicsTextItem	* textItem_m;
   // ------------------------------------------------------------------------------------------------------
public:
	edgeItemData (	QEdgeItem * graphicsItem_p = NULL, double length_p = 13,
					   const QColor   & lineColor_p = Qt::black,
					   const QString  & text_p = QString(), 
					   const QFont    & font_p = QFont( "Times" ) );
   // ------------------------------------------------------------------------------------------------------

	edgeItemData ( const edgeItemData & strctEtalon_cp ) :
		beginPoint_m   ( strctEtalon_cp.beginPoint_m	   ),
		endPoint_m		( strctEtalon_cp.endPoint_m	   ),
		length_m       ( strctEtalon_cp.length_m	      ),
		angle_m			( strctEtalon_cp.angle_m			),
		lineColor_m		( strctEtalon_cp.lineColor_m		),
      graphicsItem_m	( strctEtalon_cp.graphicsItem_m	),
		textItem_m		( strctEtalon_cp.textItem_m		)
	{	}
   // ------------------------------------------------------------------------------------------------------

   ~edgeItemData () {}
   // ------------------------------------------------------------------------------------------------------

   const QEdgeItem * item () const { return graphicsItem_m; }
   // ------------------------------------------------------------------------------------------------------

   QEdgeItem * item () { return graphicsItem_m; }
   // ------------------------------------------------------------------------------------------------------

	bool operator == ( const edgeItemData & strctEtalon_p ) const { return false; }
   // ------------------------------------------------------------------------------------------------------
}; // class edgeItemData
// ---------------------------------------------------------------------------------------------------------

class QEdgeItem : public QGraphicsItem, public utl::edge< dragBallData, edgeItemData >
{
public:
   typedef utl::edge< dragBallData, edgeItemData > edgeParent_class;
   // ------------------------------------------------------------------------------------------------------
private:
	enum eZvalues { Z_VALUE_EDGE = 3 };
   // ------------------------------------------------------------------------------------------------------
   const static unsigned int piInDegrees_scm = 180;
   // ------------------------------------------------------------------------------------------------------

	static double pi() {	return 3.1415926535897932384626433832795; }
	// ------------------------------------------------------------------------------------------------------

	static double pi_div2() { static const double result_scl = pi() / 2; return result_scl; }
   // ------------------------------------------------------------------------------------------------------

	static double pi_div3() { static const double result_scl = pi() / 3; return result_scl; }
   // ------------------------------------------------------------------------------------------------------

	static double pi_div6() { static const double result_scl = pi() / 6; return result_scl; }
   // ------------------------------------------------------------------------------------------------------

	bool invalidData();
   // ------------------------------------------------------------------------------------------------------
public:

	QEdgeItem (	double legth_p, const QColor & color_cp = Qt::black, 
					const QString & text_cp = QString(), const QFont & font_cp = QFont( "Times" ) ) :
		QGraphicsItem( NULL ),
		edgeParent_class( edgeItemData( this, legth_p, color_cp, text_cp, font_cp ) )
	{	setZValue( Z_VALUE_EDGE ); }
   // ------------------------------------------------------------------------------------------------------

   ~QEdgeItem ()
   {	
      edgeParent_class::data().graphicsItem_m = NULL; 
      edgeParent_class::data().textItem_m = NULL;
   }
   // ------------------------------------------------------------------------------------------------------

   const QString text () const
	{	return NULL != constData().textItem_m ? constData().textItem_m->toPlainText() : QString(); }
   // ------------------------------------------------------------------------------------------------------

	double length () const { return constData().length_m; }
   // ------------------------------------------------------------------------------------------------------

	const QColor & lineColor () const { return constData().lineColor_m; }
   // ------------------------------------------------------------------------------------------------------

	void setText( const QString & text_cp, const QFont * font_cp = NULL );
   // ------------------------------------------------------------------------------------------------------

	void setLength( double value_p ) { edgeParent_class::data().length_m = value_p; }
   // ------------------------------------------------------------------------------------------------------

	void setLineColor( const QColor & color_cp ) { edgeParent_class::data().lineColor_m = color_cp; }
   // ------------------------------------------------------------------------------------------------------

	void computeSelfPoints ();
   // ------------------------------------------------------------------------------------------------------

	const QPointF computeDelta (  unsigned int distanceFactor_p, 
								         double maxPower_p, const QLineF & line_cp ) const;
   // ------------------------------------------------------------------------------------------------------

	QRectF boundingRect () const;
   // ------------------------------------------------------------------------------------------------------

	QPainterPath shape () const;
   // ------------------------------------------------------------------------------------------------------

	void paint ( QPainter * painter_p, const QStyleOptionGraphicsItem * option_p, QWidget * widget_p );
   // ------------------------------------------------------------------------------------------------------
}; // class QEdgeItem
// ---------------------------------------------------------------------------------------------------------
#endif // QT_EDGE_ITEM_H_MRV
// ---------------------------------------------------------------------------------------------------------
