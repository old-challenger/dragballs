// ---------------------------------------------------------------------------------------------------------
#ifndef QT_DRAG_BALLS_VIEW_H_MRV
#define QT_DRAG_BALLS_VIEW_H_MRV
// ---------------------------------------------------------------------------------------------------------
#include <QDragBallsScene.h>
// ---------------------------------------------------------------------------------------------------------
#include <QWidget>
#include <QMouseEvent>
#include <QGraphicsView>
#include <QGraphicsRectItem>
// ---------------------------------------------------------------------------------------------------------
#include <utility>
// ---------------------------------------------------------------------------------------------------------

class QDragBallsView : public QGraphicsView
{
private:
   bool              mouseDown_m;
	double	         scale_m;
   QPointF           mousePosition_m,
                     sceneCenter_m;
	QDragBallsScene	scene_m;
   // ------------------------------------------------------------------------------------------------------

   void checkSceneSize ();
   // ------------------------------------------------------------------------------------------------------

   void endMouseDruging ()
   {
      mouseDown_m = false;
      setCursor( Qt::OpenHandCursor );
   }
   // ------------------------------------------------------------------------------------------------------
protected:

   virtual void resizeEvent ( QResizeEvent * event_p )
   {	
      QGraphicsView::resizeEvent( event_p ); 
      checkSceneSize();
   }
   // ------------------------------------------------------------------------------------------------------

   virtual void mousePressEvent ( QMouseEvent * mouseEvent_p );
   // ------------------------------------------------------------------------------------------------------

   virtual void mouseMoveEvent ( QMouseEvent * mouseEvent_p );
   // ------------------------------------------------------------------------------------------------------

   virtual void mouseReleaseEvent ( QMouseEvent * mouseEvent_p )
   {
      if ( mouseDown_m ) endMouseDruging();
      QGraphicsView::mouseReleaseEvent( mouseEvent_p );
   }
   // ------------------------------------------------------------------------------------------------------
public:

	QDragBallsView ( QWidget * parent_p = NULL );
   // ------------------------------------------------------------------------------------------------------

	const QDragBallsScene & constScene () const { return scene_m; }
   // ------------------------------------------------------------------------------------------------------

	QDragBallsScene & scene () { return scene_m; }
   // ------------------------------------------------------------------------------------------------------

	double scalePercents () const { return scale_m; }
   // ------------------------------------------------------------------------------------------------------

	void setScalePercents ( double percents_p )
	{
		const double factor_cl = percents_p / scale_m;
		scale( factor_cl, factor_cl ); 
		scale_m = percents_p;
	}
   // ------------------------------------------------------------------------------------------------------
}; // class QDragBallsView
// ---------------------------------------------------------------------------------------------------------
#endif // QT_DRAG_BALLS_VIEW_H_MRV
// ---------------------------------------------------------------------------------------------------------
